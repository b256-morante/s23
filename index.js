let trainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ["Pikachu" , "Bulbasaur", "Squirtle", "Charmander"],
	friends : { hoenn:["May" , "Max"] , kanto:["Brock" , "Misty"]},

	talk: function() {
		console.log("Pikachu I choose you!");
	}
}


console.log('Result from dot notation:');
console.log(trainer.name);

console.log(trainer);
console.log('Result of talk method:');
trainer.talk();

console.log('Result from square bracket notation:');
console.log(trainer['pokemon']);


/*
function goPokemon(name, level){

	this.name = name;
	this.level = level;

	let health = this.level * 2;
	let attack = this.level;

	this.tackle = function(){

		this.health - this.attack

	}
};



let geodude = new goPokemon('Pikachu', 12);
let mewtwo = new goPokemon('geodude', 23);

console.log(geodude);
console.log(mewtwo);

function attackPokemon(health, attack){

	this.health = health;
	this.level = level;


}
console.log("geodude tackled Pikachu");
geodude.tackle();
console.log("Pikachu's health")

*/

  function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;

  this.tackle = function(target) {
    target.health -= this.attack;
    console.log(this.name + " tackled " + target.name + "! ");
    console.log(target.name + " is now reduced to " + target.health + " health points.");
    if (target.health <= 0) {
      target.faint();
    }
  };

  this.faint = function() {
    console.log(this.name + " fainted.");
  };
}


  let Geodude = new Pokemon('Geodude', 12);
  let Pikachu = new Pokemon('Pikachu', 20);

  Geodude.tackle(Pikachu);
  console.log(Geodude.tackle(Pikachu));
